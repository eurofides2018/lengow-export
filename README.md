# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Magento is an ecommerce solution that allows merchants to set up their own online stores. Particularly well-suited to SMEs and used by over 240,000 merchants around the world, the solution is available in an open source version for small stores or in a corporate version for merchants with a broader product catalogue.

    Easily export your products from your Magento store, then select from among our 1,800 marketing channels to publish your product catalogue.
    Test all of your active feeds directly from your Magento back office.
    Import and manage your orders via your Magento interface.
    Monitor your performances via a dashboard that encompasses click stats, sales, ROI, etc.
* 1.0
* (https://www.lengow.com/plugins/magento/)

### How do I get set up? ###

* This module is compatible with all Magento versions from 1.5 and 1.9.

* It is preferable, initially, to disable your old module, then confirm installation of the new module and the exported source before uninstalling it completely.


### Contribution guidelines ###

* Once the installation is complete, log off and log on again to your Magento back office in order to reload the user rights. Remember to clear the Magento caches too.

    You should see a new Lengow menu in the upper horizontal menu.

    Another Lengow menu is equally visible in the System > Configuration page.

### Who do I talk to? ###

* Visit our Help Center for information on how to configure your Magento module
